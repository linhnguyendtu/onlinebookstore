package com.demo1.onlinebookstoredemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Onlinebookstoredemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(Onlinebookstoredemo1Application.class, args);
    }

}
